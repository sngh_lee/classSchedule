Page({
  /**
   * 页面的初始数据
   */
  data: {
    startDate: '',
    endDate: '',
    remindTypeId: null,
    remindTypeIndex: null,
    remindTime: '',
    remindType: ["提前一天提醒", "提前两天提醒", "提前三天提醒", "提前四天提醒", "提前五天提醒", "提前一周提醒"],
    remindTypeObj: [
      { id: "1", label: "提前一天提醒" },
      { id: "2", label: "提前两天提醒" },
      { id: "3", label: "提前三天提醒" },
      { id: "4", label: "提前四天提醒" },
      { id: "5", label: "提前五天提醒" },
      { id: "6", label: "提前一周提醒" }
    ]
  },
  //保存课表信息
  saveScheduleData: function (e) {
    console.log("提交参数：");
    console.log(e.detail.value);
  },
  //设置提醒方式
  setRemindType: function (e) {
    this.setData({
      remindTypeIndex: e.detail.value
    });
  },
  //设置提醒时间
  setRemindTime: function(e){
    this.setData({
      remindTime: e.detail.value
    });
  },
  //设置开始日期
  setStartDate: function (e) {
    this.setData({
      startDate: e.detail.value
    });
  },
  //设置结束日期
  setEndDate: function (e) {
    this.setData({
      endDate: e.detail.value
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})