//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    scheduleId:null,//记录课表id
    colorArrays: ["#85B8CF", "#90C652", "#D8AA5A", "#FC9F9D", "#0A9A84", "#61BC69", "#12AEF3", "#E29AAD"],
    wlist: [
      { "xqj": 1, "skjc": 1, "skcd": 1, "kcmc": "语文" },
      { "xqj": 1, "skjc": 2, "skcd": 1, "kcmc": "语文" },
      { "xqj": 1, "skjc": 3, "skcd": 1, "kcmc": "语文" },
      { "xqj": 1, "skjc": 4, "skcd": 1, "kcmc": "语文" },
      { "xqj": 1, "skjc": 5, "skcd": 1, "kcmc": "英语" },
      { "xqj": 2, "skjc": 1, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 2, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 3, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 4, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 5, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 6, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 7, "skcd": 1, "kcmc": "体育" },
      { "xqj": 2, "skjc": 8, "skcd": 1, "kcmc": "数学" },
      { "xqj": 3, "skjc": 4, "skcd": 1, "kcmc": "看图写话" },
      { "xqj": 3, "skjc": 8, "skcd": 1, "kcmc": "自然" },
      { "xqj": 3, "skjc": 5, "skcd": 1, "kcmc": "小机器人" },
      { "xqj": 4, "skjc": 2, "skcd": 1, "kcmc": "劳动" },
      { "xqj": 4, "skjc": 8, "skcd": 1, "kcmc": "托管" },
      { "xqj": 5, "skjc": 1, "skcd": 1, "kcmc": "吉他" },
      { "xqj": 6, "skjc": 3, "skcd": 1, "kcmc": "乒乓球" },
      { "xqj": 7, "skjc": 5, "skcd": 1, "kcmc": "篮球" },
    ]
  },
  loadCourseList: function(){

  },
  onLoad: function (options) {
    console.log(options);
    this.setData({
      scheduleId: options.id
    });
  }
})
