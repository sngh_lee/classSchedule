Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrolltop: null, //滚动位置
    scheduleList: [], //课表列表数据
    page: 0  //分页
  },
  scrollHandle: function (e) { //滚动事件
    this.setData({
      scrolltop: e.detail.scrollTop
    })
  },
  scrollLoading: function () { //滚动加载
    this.loadScheduleList();
  },
  //加载课表列表数据
  loadScheduleList: function () {
    let _this = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    const perpage = 10;
    this.setData({
      page: this.data.page + 1
    })
    const page = this.data.page;
    const newlist = [];
    for (var i = (page - 1) * perpage; i < page * perpage; i++) {
      newlist.push({
        "id": i + 1,
        "name": "测试课表" + (i + 1),
        "tag": "法律咨询",
      })
    };
    console.log(newlist);
    setTimeout(() => {
      _this.setData({
        scheduleList: _this.data.scheduleList.concat(newlist)
      })
    }, 1500);
    console.log(_this.data.scheduleList);
  },
  //点击删除按钮处理函数
  deleteSchedule: function(e){
    console.log(e);
    let _this=this;
    let id=e.target.dataset.id;
    wx.showModal({
      title: '',
      content: '确定要删除此课表？',
      cancelText: '取消',
      confirmText: '确定',
      success: function(res){
        if (res.confirm) {
          console.log('用户点击确定');
          _this.deleteScheduleData(id);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  //删除课表数据
  deleteScheduleData: function(id){
    console.log("删除课表数据：id="+id);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadScheduleList();//加载课表列表
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.loadScheduleList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadScheduleList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})