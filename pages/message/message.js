Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrolltop: null, //滚动位置
    messageList: [], //消息列表数据
    page: 0  //分页
  },
  scrollHandle: function (e) { //滚动事件
    this.setData({
      scrolltop: e.detail.scrollTop
    })
  },
  scrollLoading: function () { //滚动加载
    this.loadMessageList();

    this.setData({
      scrolltop: e.detail.scrollTop
    })
  },
  scrollLoading: function () { //滚动加载
    this.loadMessageList();
  },
  //加载课表列表数据
  loadMessageList: function () {
    let _this = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    const perpage = 10;
    this.setData({
      page: this.data.page + 1
    })
    const page = this.data.page;
    const newlist = [];
    for (var i = (page - 1) * perpage; i < page * perpage; i++) {
      newlist.push({
        "id": i + 1,
        "message": "下周三英语课考单词" + (i + 1)
      })
    };
    console.log(newlist);
    setTimeout(() => {
      _this.setData({
        messageList: _this.data.messageList.concat(newlist)
      })
    }, 1500);
    console.log(_this.data.messageList);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadMessageList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.loadMessageList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})